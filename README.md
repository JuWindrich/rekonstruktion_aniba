# Rekonstruktion_Aniba

Zusammenstellung der Ergebnisse der Masterarbeit "Das versunkene Aniba - Methoden zur Rekonstruktion versunkener archäologischer Landschaften". Die Masterarbeit wurde im Studiengang „Digitale Methodik in den Geistes- und Kulturwissenschaften“ geschrieben. Ihr Ziel war die Rekonstruktion der Landschaft und Monumente von Aniba und Qasr Ibrim in einem GIS-Projekt. Diese Rekonstruktion ist in Karten festgehalten, um Ägyptologen bei der Forschung zu unterstützen.

Die Inhalte sind unter der Creative Commons Namensnennung – Weitergabe unter gleichen
Bedingungen 4.0 International License lizensiert. Eine Kopie dieser Lizenz ist unter dem
folgenden Link zu finden: http://creativecommons.org/licenses/by-sa/4.0/deed.de.
