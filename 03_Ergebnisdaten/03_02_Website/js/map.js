// Karte. Gradangabe in WGS84
let mymap = L.map('mapid').setView([22.6737, 32.0147], 13);

// Auswahl, welche "Karte"(layer) man drüber legen will (OpenStreetMap, etc.)
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

function onEachFeature(feature, layer) {
    // does this feature have a property named popupContent?
    layer.bindPopup(feature.properties.Aniba_Monumente_NamedesMonuments);
}

var myStyle = {
  "color" : "#729FCF"
}

L.geoJSON(monumente, {
  onEachFeature: onEachFeature,
  style : myStyle
}).addTo(mymap);
